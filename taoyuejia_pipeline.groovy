//驾值网打包脚本
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/local/java1.8/jdk1.8.0_271'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        MAVEN_HOME = '/usr/local/maven'

        SYS_PATH = "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"
        JAVA_PATH = "${JAVA_HOME}/bin:${JRE_HOME}/bin"
        ANDROID_PATH = "${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
        FLUTTER_PATH = "${FLUTTER_HOME}/bin:${FLUTTER_HOME}/bin/cache/dart-sdk/bin"
        PATH = "${env.PATH}:${SYS_PATH}:${FLUTTER_PATH}:${MAVEN_HOME}/bin:${JAVA_PATH}:${ANDROID_PATH}"
        //蒲公英安装链接
        pgyer = ' '
        //二维码链接
        qrCodeUrl = ' '
        versionName = ' '
        versionCode = ' '
        credentialsId="a20b1e50-4534-4dcc-a9a7-b820957071f5"
    }
    parameters {
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', name: 'taoyuejia_branch', type: 'PT_BRANCH', useRepository: '.*taoyuejia.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', name: 'flutter_branch', type: 'PT_BRANCH', useRepository: '.*taoyuejia_flutter.git'
        choice(name: 'type', choices: ['qa', 'prod'], description: '构建环境')
        booleanParam(name: 'sendRobot', defaultValue: true, description: '是否发送到打包群')
        booleanParam(name: 'uploadCos', defaultValue: false, description: '上传到腾讯云（prod时勾选，必须确认版本号，否则会覆盖线上包！！！）')
        string(name: 'remark', description: '请输入本次打包修改的内容')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号，用英文逗号分隔,chensisi,liusai,mamingchen,wangquan,anyingying,xulanzhong)')
    }
    stages {
        stage('Init Code') {
            steps {
                dir('taoyuejia') {
                    git branch: "${params.taoyuejia_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/taoyuejia.git'
                }
                dir('taoyuejia_flutter') {
                    git branch: "${params.flutter_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App/Flutter/taoyuejia_flutter.git'
                }
                dir('taoyuejia') {
                    script {
                        versionName = sh(script: "cat app/build.gradle | awk -F\\\" '/versionName/{print \$2}'", returnStdout: true).trim()
                        versionCode = sh(script: "cat app/build.gradle | awk '/versionCode/{print \$2}'", returnStdout: true).trim()
                        println versionName
                        println versionCode
                    }        
                }
            }
        }
        stage('Build flutter') {
            steps {
                dir('taoyuejia') {
                    sh "chmod +x util/2_updateFlutter.sh"
                    sh "util/2_updateFlutter.sh"
                    sh "chmod +x util/3_buildFlutter.sh"
                    sh "util/3_buildFlutter.sh"
                }
            }
        }
        stage('Build taoyuejia') {
            steps {
                dir('taoyuejia') {
                    script {
                        echo '打包'
                        sh './gradlew clean'
                        //打包
                        if (params.type == 'qa') {
                            sh './gradlew app:assembleDebug'
                        } else {
                            sh './gradlew app:assembleRelease'
                        }
                    }
                }
            }
        }
        stage('上传 apk') {
            parallel {
                stage('上传蒲公英') {
                    steps {
                        script {
                            echo '上传蒲公英'
                            //上传到蒲公英
                            def apkPath = ''
                            if (params.type == 'qa') {
                                apkPath = "taoyuejia/app/build/outputs/apk/debug/app-debug.apk"
                            } else {
                                apkPath = "taoyuejia/app/build/outputs/apk/release/app-release.apk"
                            }
                            //蒲公英key
                            def uKey = "15df770f490d1715e71300ea22cc2ab0"
                            def apiKey = "9dc766c341bd376a74dbb582518d6db6"
                            //蒲公英上传地址
                            def url = "https://www.pgyer.com/apiv2/app/upload"
                            def response = sh(script: "curl -F 'file=@${apkPath}' -F 'uKey=$uKey' -F '_api_key=$apiKey' $url", returnStdout: true).trim()
                            println response
                            def jsonSlurper = new groovy.json.JsonSlurper()
                            def resultData = jsonSlurper.parseText(response).data
                            pgyer = "https://www.pgyer.com/apiv2/app/install?_api_key=$apiKey&buildKey=${resultData.buildKey}"
                            qrCodeUrl = resultData.buildQRCodeURL
                        }
                    }
                }
                
                stage('上传本地') {
                    steps {
                        dir('taoyuejia') {
                            script {
                                def apkPath = ''
                                if (params.type == 'qa') {
                                    apkPath = "app/build/outputs/apk/debug/app-debug.apk"
                                } else {
                                    apkPath = "app/build/outputs/apk/release/app-release.apk"
                                }
                                //上传腾讯云
                                if(params.uploadCos) {
                                    def target = "/taoyuejia/${type}/taoyuejia-${type}-${versionName}-${versionCode}.apk";
                                    sh "node util/5_uploadCos.js ${apkPath} ${target}"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            dir('taoyuejia') {
                //还原taoyuejia
                sh 'git checkout .'
            }
            dir('taoyuejia_flutter') {
                //还原flutter
                sh 'git checkout .'
            }
        }
        success {
            script {
                //归档
                def apkDir = "taoyuejia/app/build/outputs/apk"
                def apkPath = ''
                if (params.type == 'qa') {
                    apkPath = "${apkDir}/debug/app-debug.apk"
                } else {
                    apkPath = "${apkDir}/release/app-release.apk"
                }
                def cosPath = "${apkDir}/taoyuejia-${params.type}-${versionName}-${versionCode}.apk"
                sh "cp ${apkPath} ${cosPath}"

                //生成apk地址二维码
                def apkQrCodePath = "taoyuejia/app/build/outputs/apkUrlQrcode.png"
                def apkJenkinsUrl = "${env.BUILD_URL}artifact/${cosPath}"
                sh "qr '${apkJenkinsUrl}' > ${apkQrCodePath}"
                archiveArtifacts artifacts: "${cosPath},${apkQrCodePath}", fingerprint: true, onlyIfSuccessful: true
                
                def apkQrCodeUrl = "${env.BUILD_URL}artifact/${apkQrCodePath}"
                println apkJenkinsUrl
                println apkQrCodeUrl
            }
            script {
                echo 'success'
                def branchInfo = "taoyuejia分支：${params.taoyuejia_branch}\nflutter分支：${params.flutter_branch}\n环境：${params.type}"
                def versionInfo = "version：${versionName}#${versionCode}"
                def installInfo = "<a href=\"${pgyer}\">点击直接安装</a>\n\n<a href=\"${qrCodeUrl}\">点击查看二维码</a>"
                def msg = "${branchInfo}\n${versionInfo}\n${installInfo}"
                if(params.uploadCos) {
                    def target = "/taoyuejia/${type}/taoyuejia-${type}-${versionName}-${versionCode}.apk";
                    def apkUrl = "https://taoche-res.taoche.com${target}"
                    msg = "${msg}\n\n服务器下载地址：${apkUrl}"
                }
                def postData = groovy.json.JsonOutput.toJson(["appId" : "c.111111", "args":"1000006", "subject" : "淘悦驾Android打包成功", "text":"${msg}", "wxusers" : "${params.wxusers}"])
                postData = postData.replaceAll("'", "'\\\\''")
                sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"

                //机器人通知
                if(params.sendRobot) {
                    def content = "### 淘悦驾打包成功Android打包成功\n" +
                        "${branchInfo}" +
                        "环境：<font color=\"warning\">${params.type}</font>\n" +
                        "version：<font color=\"info\">${versionName}#${versionCode}</font>\n" +
                        "##### [点击直接安装](${pgyer})\n" +
                        "##### [点击查看二维码](${qrCodeUrl})\n"
                    if(params.uploadCos) {
                        content = "${content}\n##### 服务器下载地址：${apkUrl}"
                    }
                    def markdown = groovy.json.JsonOutput.toJson(["msgtype" : "markdown", "markdown": ["content": "$content"]]).replaceAll("'", "'\\\\''")
                    //sh "curl -H 'Content-Type:application/json' -d '$wechat' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=26c984b5-3992-49b5-a3ae-da6e616d4a35'"
                    sh "curl -H 'Content-Type:application/json' -d '$markdown' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ae3604f0-7fbe-4255-bebe-4fc003ea1a47'"

                    //@相应人员
                    def remark = ''
                    if(params.remark) {
                        remark = "本次更新内容：\n${params.remark}"
                    } else {
                        remark = "更新日志：\n${getChangeLogs()}"
                    }
                    def mentionedUsers = params.wxusers.split(",")
                    def text = groovy.json.JsonOutput.toJson(["msgtype" : "text", "text": ["content": "$remark", "mentioned_list": mentionedUsers]])
                    sh "curl -H 'Content-Type:application/json' -d '$text' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ae3604f0-7fbe-4255-bebe-4fc003ea1a47'"
                }
            }
        }
        failure {
            script {
                echo 'failure'
                def branchInfo = "taoyuejia分支：${params.taoyuejia_branch}\\nflutter分支：${params.flutter_branch}\\n环境：${params.type}"
                def versionInfo = "version：${versionName}#${versionCode}"
                def msg = "${branchInfo}\\n${versionInfo}\\n请检查失败原因"
                def postData = '{"appId":"c.111111","subject":"淘悦驾打包失败","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
    }
}

@NonCPS
//git上提交的修改日志
def getChangeLogs() {
    def changeLogs = ""
    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            changeLogs += "* ${entry.msg} by ${entry.author} \n"
        }
    }
    return changeLogs
}