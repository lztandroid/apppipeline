//长传网络包到腾讯云桶
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/local/java1.8/jdk1.8.0_271'
        JRE_HOME = "${JAVA_HOME}/jre"
        JAVA_PATH = "${JAVA_HOME}/bin:${JRE_HOME}/bin"
        SYS_PATH = "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"
        PATH = "${env.PATH}:${SYS_PATH}:${JAVA_PATH}"
        credentialsId = "a20b1e50-4534-4dcc-a9a7-b820957071f5"
        //cos路径
        cosFileUrl = ' '
    }
    parameters {
        choice(name: 'fileType', choices: ['小马达Android', '淘悦驾Android', '通用'], description: '上传文件类型')
        string(name: 'fileUrl', description: '请输入文件网络地址')
        string(name: 'target', description: 'COS路径，默认使用文件名')
        string(name: 'wxusers', defaultValue: 'lixianpeng,wangsen', description: '通知账号(域账号，用英文逗号分隔)')
    }
    stages {
        stage('Init Code') {
            steps {
                git branch: "master", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/lixianpeng/marstools.git'
            }
        }
        stage('Upload Cos') {
            steps {
                script {
                    def target = ''
                    if(params.target) {
                        target = params.target
                    } else {
                        def fileName = params.fileUrl.substring(params.fileUrl.lastIndexOf("/") + 1)
                        if(params.fileType == "小马达Android") {
                            target = "/marscar/prod/${fileName}"
                        } else if(params.fileType == "淘悦驾Android") {
                            target = "/taoyuejia/prod/${fileName}"
                        } else {
                            target = "/common/${fileName}"
                        }
                    }
                    //上传腾讯云
                    sh "python3 python/uploadCos.py --path=${params.fileUrl} --target=${target}"
                    cosFileUrl = "https://taoche-res.taoche.com${target}"
                }
            }
        }
    }
    post {
        success {
            script {
                echo 'success'
                def msg = "下载地址:\n${cosFileUrl}"
                def postData = groovy.json.JsonOutput.toJson(["appId" : "c.111111", "args":"1000006", "subject" : "文件上传成功", "text":"${msg}", "wxusers" : "${params.wxusers}"])
                sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
        failure {
            script {
                echo 'failure'
                def msg = "fileType:${params.fileType};url:${params.fileUrl}"
                def postData = '{"appId":"c.111111","subject":"上传失败","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
    }
}