//小马达组件打包脚本
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/local/java1.8/jdk1.8.0_271'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        MAVEN_HOME = '/usr/local/maven'
        PATH = "${env.PATH}:$FLUTTER_HOME/bin:$FLUTTER_HOME/bin/cache/dart-sdk/bin:$MAVEN_HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin:${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
      
        // 主项目目录
        mars_path = 'MarsCar'
        // 门店项目目录
        store_path = 'MarsStore'
        // 金融项目目录
        financial_path = 'MarsFinancial'
        // flutter项目目录
        flutter_path = 'taoche_mars_flutter'
        // 会员项目目录
        member_path = 'marsmember'
        // c2b项目目录
        c2b_path = 'MarsC2B'

        // 门店版本号
        store_version = ' '
        // 金融版本号
        financial_version = ' '
        // flutter版本号
        flutter_version = ' '
        // 会员版本号
        member_version = ' '
        // c2b版本号
        c2b_version = ' '

        // 二维码链接
        qrCodeUrl = ' '
        //本地带版本信息路径
        cosPath = ' '
        credentialsId="a20b1e50-4534-4dcc-a9a7-b820957071f5"
    }
    parameters {
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'release', selectedValue:'DEFAULT', name: 'mars_branch', type: 'PT_BRANCH', useRepository: '.*MarsCar.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', selectedValue:'DEFAULT', name: 'store_branch', type: 'PT_BRANCH', useRepository: '.*MarsStore.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', selectedValue:'DEFAULT', name: 'financial_branch', type: 'PT_BRANCH', useRepository: '.*MarsFinancial.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', selectedValue:'DEFAULT', name: 'flutter_branch', type: 'PT_BRANCH', useRepository: '.*taoche_mars_flutter.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', selectedValue:'DEFAULT', name: 'member_branch', type: 'PT_BRANCH', useRepository: '.*marsmember.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', selectedValue:'DEFAULT', name: 'c2b_branch', type: 'PT_BRANCH', useRepository: '.*marsc2b.git'
        text(name: 'otherImplement', defaultValue: '', description: '自定义引用模块(使用换行分隔)')
        choice(name: 'type', choices: ['qa', 'prod'], description: '构建环境')
        booleanParam(name: 'sendRobot', defaultValue: true, description: '是否发送到打包群')
        booleanParam(name: 'uploadCos', defaultValue: false, description: '上传到腾讯云（prod时勾选，必须确认版本号，否则会覆盖线上包！！！）')
        string(name: 'remark', description: '请输入本次打包修改的内容')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号,用英文逗号分隔,chensisi,liusai,mamingchen,wangquan,anyingying,xulanzhong)')
    }
    stages {
        stage('Init Code') {
            steps {
                dir("${mars_path}") {
                    git branch: "${params.mars_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/MarsCar.git'
                    script {
                        if (params.type == 'qa') {
                            //修改版本号
                            def oldVersionCode = getVersionCode()
                            def newVersionCode = oldVersionCode.toInteger() + 1
                            println "${oldVersionCode} -> ${newVersionCode}"
                            sh "sed -i 's/versionCode.*/versionCode ${newVersionCode}/' app/build.gradle"
                            def newVersionName = "${randVersion(10,99)}.${randVersion(0,9)}.${randVersion(0,9)}"
                            println "newVersionName -> ${newVersionName}"
                            sh "sed -i 's/versionName.*/versionName \"${newVersionName}\"/' app/build.gradle"
                        }
                    }
                }
                dir("${store_path}") {
                    git branch: "${params.store_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/MarsStore.git'
                }
                dir("${financial_path}") {
                    git branch: "${params.financial_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/MarsFinancial.git'
                }
                dir("${flutter_path}") {
                    git branch: "${params.flutter_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App/Flutter/taoche_mars_flutter.git'
                }
                dir("${member_path}") {
                    git branch: "${params.member_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/marsmember.git'
                }
                dir("${c2b_path}") {
                    git branch: "${params.c2b_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/marsc2b.git'
                }
            }
        }
        stage('Build Projects') {
            parallel {
                stage('Build Store') {
                    when { 
                        expression { return params.store_branch != 'master' }
                    }
                    steps {
                        dir("${store_path}") {
                            script {
                                sh './gradlew clean'
                                //打包
                                if (params.type == 'qa') {
                                    sh './gradlew module_erp:publishForTest'
                                } else {
                                    sh './gradlew module_erp:publishStrict'
                                }
                                store_version = sh(script: "cat */module_version.txt", returnStdout: true).trim()
                            }
                        }
                        dir("${mars_path}") {
                            script {
                                def lineNum = sh(script: 'sed -n \'/implementation/=\' app/build.gradle | sed -n "$"p', returnStdout: true).trim()
                                sh "sed -i \"${lineNum}a\\implementation '${store_version}'\" app/build.gradle"
                            }
                        }
                    }
                }
                stage('Build Financial') {
                    when { 
                        expression { return params.financial_branch != 'master' }
                    }
                    steps {
                        dir("${financial_path}") {
                            script {
                                sh './gradlew clean'
                                //打包
                                if (params.type == 'qa') {
                                    sh './gradlew module_mars:publishForTest'
                                } else {
                                    sh './gradlew module_mars:publishStrict'
                                }
                                financial_version = sh(script: "cat module_mars/module_version.txt", returnStdout: true).trim()
                            }
                        }
                        dir("${mars_path}") {
                            script {
                                def lineNum = sh(script: 'sed -n \'/implementation/=\' app/build.gradle | sed -n "$"p', returnStdout: true).trim()
                                sh "sed -i \"${lineNum}a\\implementation '${financial_version}'\" app/build.gradle"
                            }
                        }
                    }
                }
                stage('Build Flutter') {
                    when { 
                        expression { return params.flutter_branch != 'master' }
                    }
                    steps {
                        dir("${flutter_path}") {
                            script {
                                //打包
                                if (params.type == 'qa') {
                                    sh 'sh archive/buildFlutter.sh'
                                } else {
                                    sh 'sh archive/buildFlutter.sh release'
                                }
                                flutter_version = sh(script: "cat module_version.txt", returnStdout: true).trim()
                            }
                        }
                        dir("${mars_path}") {
                            script {
                                def lineNum = sh(script: 'sed -n \'/implementation/=\' app/build.gradle | sed -n "$"p', returnStdout: true).trim()
                                sh "sed -i \"${lineNum}a\\implementation '${flutter_version}'\" app/build.gradle"
                            }
                        }
                    }
                }
                stage('Build Member') {
                    when { 
                        expression { return params.member_branch != 'master' }
                    }
                    steps {
                        dir("${member_path}") {
                            script {
                                sh './gradlew clean'
                                //打包
                                if (params.type == 'qa') {
                                    sh './gradlew publishForTest'
                                } else {
                                    sh './gradlew publishStrict'
                                }
                                member_version = sh(script: "cat */module_version.txt", returnStdout: true).trim()
                            }
                        }
                        dir("${mars_path}") {
                            script {
                                def lineNum = sh(script: 'sed -n \'/implementation/=\' app/build.gradle | sed -n "$"p', returnStdout: true).trim()
                                sh "sed -i \"${lineNum}a\\implementation '${member_version}'\" app/build.gradle"
                            }
                        }
                    }
                }
                stage('Build C2B') {
                    when { 
                        expression { return params.c2b_branch != 'master' }
                    }
                    steps {
                        dir("${c2b_path}") {
                            script {
                                sh './gradlew clean'
                                //打包
                                if (params.type == 'qa') {
                                    sh './gradlew publishForTest'
                                } else {
                                    sh './gradlew publishStrict'
                                }
                                c2b_version = sh(script: "cat */module_version.txt", returnStdout: true).trim()
                            }
                        }
                        dir("${mars_path}") {
                            script {
                                //在MarsCar app/build.gradle找到最后一个引用类库的行数
                                def lineNum = sh(script: 'sed -n \'/implementation/=\' app/build.gradle | sed -n "$"p', returnStdout: true).trim()
                                //添加引用implementation到最后一行
                                sh "sed -i \"${lineNum}a\\implementation '${c2b_version}'\" app/build.gradle"
                            }
                        }
                    }
                }
            }
        }
        stage('Build Mars') {
            when {  expression { return true } }
            steps {
                dir("${mars_path}") {
                    script {
                        //添加自定义引用
                        if(!params.otherImplement.trim().isEmpty()) {
                            def otherImplement = params.otherImplement.replaceAll("\n", "\\\\n").replaceAll("'", "'\\\\''")
                            def lineNum = sh(script: 'sed -n \'/implementation/=\' app/build.gradle | sed -n "$"p', returnStdout: true).trim()
                            sh "sed -i '${lineNum}a\\${otherImplement}' app/build.gradle"
                        }

                        sh "cat app/build.gradle"
                        //删除原来的文件
                        sh "rm -rf app/build/outputs/apk"
                        sh './gradlew clean'
                        //打包
                        if (params.type == 'qa') {
                            sh './gradlew app:assembleQaRelease'
                        } else {
                            sh './gradlew app:packageApkProdRelease'
                        }

                        //归档
                        def apkDir = "app/build/outputs/apk"
                        def apkPath = "${apkDir}/${params.type}/release/app-${params.type}-release.apk"
                        cosPath = "${apkDir}/${params.type}/release/marscar-${type}-${versionName}-${versionCode}.apk"
                        sh "cp ${apkPath} ${cosPath}"
                        archiveArtifacts artifacts: "${cosPath}", fingerprint: false, onlyIfSuccessful: true
                        def apkJenkinsUrl = "${env.BUILD_URL}artifact/${cosPath}"
                        println apkJenkinsUrl
                    }
                }
            }
        }
        stage('上传 apk') {
            when {  expression { return true } }
            parallel {
                stage('上传蒲公英') {
                    when {  expression { return false } }
                    steps {
                        script {
                            echo '上传蒲公英'
                            //上传到蒲公英
                            def apkDir = "${mars_path}/app/build/outputs/apk"
                            def apkPath = "${apkDir}/${params.type}/release/app-${params.type}-release.apk"
                            //蒲公英key
                            def uKey = "15df770f490d1715e71300ea22cc2ab0"
                            def apiKey = "9dc766c341bd376a74dbb582518d6db6"
                            //蒲公英上传地址
                            def url = "https://www.pgyer.com/apiv2/app/upload"
                            def response = sh(script: "curl -F 'file=@${apkPath}' -F 'uKey=$uKey' -F '_api_key=$apiKey' $url", returnStdout: true).trim()
                            println response
                            def jsonSlurper = new groovy.json.JsonSlurper()
                            def resultData = jsonSlurper.parseText(response).data
                            qrCodeUrl = resultData.buildQRCodeURL
                        }
                    }
                }
                
                stage('上传本地') {
                    steps {
                        dir("${mars_path}") {
                            script {
                                echo '上传本地服务器'
                                //上传本地服务器
                                def url = "http://10.8.100.62:3000/api/apk/uploadApk"
                                def resUploadApk = sh(script: "curl -F 'file=@${cosPath}' $url", returnStdout: true).trim()
                                println resUploadApk
                                def apkUrl = (new groovy.json.JsonSlurper()).parseText(resUploadApk).data
                                println apkUrl

                                //生成二维码
                                def qrcodeRequest = "curl \"http://10.8.100.62:3000/api/common/qrcode?type=0&app=mars&content=${apkUrl}\""
                                def resQrCode = sh(script: "${qrcodeRequest}", returnStdout: true).trim()
                                qrCodeUrl = (new groovy.json.JsonSlurper()).parseText(resQrCode).data

                                //上传腾讯云
                                if(params.uploadCos) {
                                    dir('util') {
                                        def out = sh(script:"ls node_modules", returnStatus:true)
                                        //文件夹不存在
                                        if(out == 2) {
                                            sh "npm install"
                                        }
                                    }
                                    def target = "/marscar/${type}/marscar-${type}-${versionName}-${versionCode}.apk"
                                    sh "node util/5_uploadCos.js ${apkPath} ${target}"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                echo 'always'
            }
        }

        success {
            echo 'success'
            dir("${mars_path}") {
                script {
                    def versionName = getVersionName()
                    def versionCode = getVersionCode()

                    //企业微信通知
                    def branchInfo = getBranchInfo()
                    def buildType = "环境：${params.type}"
                    def versionInfo = "version：${versionName}#${versionCode}"
                    def installInfo = "<a href=\"${qrCodeUrl}\">点击查看二维码</a>"
                    def msg = "${branchInfo}${buildType}\n${versionInfo}\n${installInfo}"
                    def target = "/marscar/${type}/marscar-${type}-${versionName}-${versionCode}.apk"
                    def apkUrl = "https://taoche-res.taoche.com${target}"
                    if(params.uploadCos) {
                        msg = "${msg}\n服务器下载地址：${apkUrl}"
                    }
                    if(params.remark) {
                        msg = "${msg}\n本次更新内容：${params.remark}"
                    } else {
                        msg = "${msg}\n更新日志：\n${getChangeLogs()}"
                    }
                    def postData = groovy.json.JsonOutput.toJson(["appId" : "c.111111", "args":"1000006", "subject" : "小马达打包成功", "text":"${msg}", "wxusers" : "${params.wxusers}"])
                    postData = postData.replaceAll("'", "'\\\\''")
                    sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
                    
                    //机器人通知
                    if(params.sendRobot) {
                        def content = "### 小马达Android打包成功\n" +
                            "${branchInfo}" +
                            "环境：<font color=\"warning\">${params.type}</font>\n" +
                            "version：<font color=\"info\">${versionName}#${versionCode}</font>\n" +
                            "##### [点击查看二维码](${qrCodeUrl})\n"
                        if(params.uploadCos) {
                            content = "${content}\n##### 服务器下载地址：${apkUrl}"
                        }
                        def markdown = groovy.json.JsonOutput.toJson(["msgtype" : "markdown", "markdown": ["content": "$content"]]).replaceAll("'", "'\\\\''")
                        //sh "curl -H 'Content-Type:application/json' -d '$wechat' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=26c984b5-3992-49b5-a3ae-da6e616d4a35'"
                        sh "curl -H 'Content-Type:application/json' -d '$markdown' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ae3604f0-7fbe-4255-bebe-4fc003ea1a47'"

                        //@相应人员
                        def remark = ''
                        if(params.remark) {
                            remark = "本次更新内容：\n${params.remark}"
                        } else {
                            remark = "更新日志：\n${getChangeLogs()}"
                        }
                        def mentionedUsers = params.wxusers.split(",")
                        def text = groovy.json.JsonOutput.toJson(["msgtype" : "text", "text": ["content": "$remark", "mentioned_list": mentionedUsers]])
                        sh "curl -H 'Content-Type:application/json' -d '$text' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ae3604f0-7fbe-4255-bebe-4fc003ea1a47'"
                    }

                    //正式环境上次打包
                    if (params.type == 'prod') {
                        sh "git commit -m 'update reboust files' -- app/robust"
                        sh "git push"
                    } else {
                        sh "git checkout ."
                    }
                }
            }
        }

        failure {
            dir("${mars_path}") {
                script {
                    echo 'failure'
                    def versionName = getVersionName()
                    def versionCode = getVersionCode()
                    def branchInfo = "门店分支：${params.store_branch}\\n金融分支：${params.financial_branch}\\nFlutter分支：${params.flutter_branch}\\n会员分支：${params.member_branch}\\nC2B分支：${params.c2b_branch}\\n"
                    if(!params.otherImplement.trim().isEmpty()) {
                        def otherImplement = params.otherImplement.trim().replaceAll("\n", "\\\\n").replaceAll("\"", "\\\\\"").replaceAll("'", "'\\\\''")
                        branchInfo += "引用模块:\\n${otherImplement}\\n"
                    }
                    branchInfo += "环境：${params.type}"
                    def versionInfo = "version：${versionName}#${versionCode}"
                    def msg = "${branchInfo}\\n${versionInfo}\\n请检查失败原因"
                    def postData = '{"appId":"c.111111","subject":"小马达打包失败","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                    sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
                    sh "git checkout ."
                }
            }
        }
    }
}

def getBranchInfo() {
    def branchInfo = ""
    if(params.store_branch != "master") {
        branchInfo += "门店分支：${params.store_branch}\n"
    }
    if(params.financial_branch != "master") {
        branchInfo += "金融分支：${params.financial_branch}\n"
    }
    if(params.flutter_branch != "master") {
        branchInfo += "Flutter分支：${params.flutter_branch}\n"
    }
    if(params.member_branch != "master") {
        branchInfo += "会员分支：${params.member_branch}\n"
    }
    if(params.c2b_branch != "master") {
        branchInfo += "C2B分支：${params.c2b_branch}\n"
    }
    if(params.otherImplement) {
        branchInfo += "引用模块:\n${params.otherImplement}\n"
    }
    return branchInfo
}

def getVersionName() {
    def versionName = sh(script: "cat app/build.gradle | awk -F\\\" '/versionName/{print \$2}'", returnStdout: true).trim()
    return versionName
}

//增加版本号
def increaseVersionName(oldVersionName) {
    def version = oldVersionName.split("\\.")
    //版本号加一
    def v2 = version[2].toInteger() + 1
    if(v2 >= 10) {
        v2 = 0
        def v1 = version[1].toInteger() + 1
        if(v1 >= 10) {
            v1 = 0
            version[0] = version[0].toInteger() + 1
        }
        version[1] = v1
    }
    version[2] = v2
    return "${version[0]}.${version[1]}.${version[2]}"
}

def getVersionCode() {
    def versionCode = sh(script: "cat app/build.gradle | awk '/versionCode/{print \$2}'", returnStdout: true).trim()
    return versionCode
}

def randVersion(min, max) {
    def random = new Random()
    return random.nextInt(max - min + 1) + min
}

@NonCPS
//git上提交的修改日志
def getChangeLogs() {
    def changeLogs = ""
    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            changeLogs += "* ${entry.msg} by ${entry.author} \n"
        }
    }
    return changeLogs
}