//易车伙伴二手车商版打包脚本
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.322.b06-1.el7_9.x86_64'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        MAVEN_HOME = '/usr/local/maven'
        PATH = "${env.PATH}:$FLUTTER_HOME/bin:$FLUTTER_HOME/bin/cache/dart-sdk/bin:$MAVEN_HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin:${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
        //项目目录
        project_path = 'taochepartner'
        //蒲公英安装链接
        pgyer = ' '
        //二维码链接
        qrCodeUrl = ' '
        versionName = ' '
        versionCode = ' '
        credentialsId="b6d92b26-2529-45ce-aa50-0401644db4d4"
    }
    parameters {
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', name: 'partner_branch', type: 'PT_BRANCH', useRepository: '.*taochepartner.git'
        choice(name: 'type', choices: ['qa', 'prodDebug', 'prod'], description: '构建环境')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号，用英文逗号分隔)')
    }
    stages {
        stage('Init Code') {
            steps {
                dir("${project_path}") {
                    git branch: "${params.partner_branch}", credentialsId: "${credentialsId}", url: 'git@gitlab.com:lztandroid/taochepartner.git'
                    script {
                        versionName = sh(script: "cat app/build.gradle | awk -F\\\" '/versionName/{print \$2}'", returnStdout: true).trim()
                        versionCode = sh(script: "cat app/build.gradle | awk '/versionCode/{print \$2}'", returnStdout: true).trim()
                        println versionName
                        println versionCode
                    }        
                }
            }
        }
        stage('打包') {
            steps {
                dir("${project_path}") {
                    script {
                        echo '打包'
                        //clean
                        //sh "./gradlew clean"
                        //打印当前目录
                        sh "pwd"
                        sh "chmod +x util/2_buildApk.sh"
                        sh "util/2_buildApk.sh ${params.type}"
                    }
                }
            }
        }
        // stage('上传 apk') {
        //     parallel {
        //         stage('上传蒲公英') {
        //             steps {
        //                 dir("${project_path}") {
        //                     script {
        //                         echo '上传蒲公英'
        //                         //上传到蒲公英
        //                         def apkPath = ''
        //                         if (params.type == 'qa') {
        //                             apkPath = "app/build/outputs/apk/qa/debug/app-qa-debug.apk"
        //                         } else if(params.type == 'prodDebug') {
        //                             apkPath = "app/build/outputs/apk/prod/releaseDebuggable/app-prod-releaseDebuggable.apk"
        //                         } else {
        //                             apkPath = "app/build/outputs/apk/prod/release/app-prod-release.apk"
        //                         }
        //                         //蒲公英key
        //                         def uKey = "09abb8c2e91b22bcb78310cc14279270"
        //                         def apiKey = "fa4877b8231dde2f8537cda2a3d63cd9"
        //                         //蒲公英上传地址
        //                         def url = "https://www.pgyer.com/apiv2/app/upload"
        //                         def response = sh(script: "curl -F 'file=@${apkPath}' -F 'uKey=$uKey' -F '_api_key=$apiKey' $url", returnStdout: true).trim()
        //                         println response
        //                         def jsonSlurper = new groovy.json.JsonSlurper()
        //                         def resultData = jsonSlurper.parseText(response).data
        //                         pgyer = "https://www.pgyer.com/apiv2/app/install?_api_key=$apiKey&buildKey=${resultData.buildKey}"
        //                         qrCodeUrl = resultData.buildQRCodeURL
        //                     }
        //                 }
        //             }
        //         }
                
        //        stage('上传本地') {
        //            steps {
        //                dir("${project_path}") {
        //                    script {
        //                        echo '上传本地服务器'
        //                        //上传本地服务器
        //                        def apkPath = ''
        //                        if (params.type == 'qa') {
        //                            apkPath = "app/build/outputs/apk/qa/debug/app-qa-debug.apk"
        //                        } else if(params.type == 'prodDebug') {
        //                            apkPath = "app/build/outputs/apk/prod/releaseDebuggable/app-prod-releaseDebuggable.apk"
        //                        } else {
        //                            apkPath = "app/build/outputs/apk/prod/release/app-prod-release.apk"
        //                        }
        //                        sh "mkdir -p 1_AutoArchive/apks"
        //                        sh "cp -rf $apkPath 1_AutoArchive/apks/app.apk"
        //                        sh "1_AutoArchive/4.1_upload_apk.sh EPPartnerAndroid"
        //                        sh "1_AutoArchive/5.1_updateHTML.sh EPPartnerAndroid 易车伙伴二手车商版 huoban_android.png $////{versionName} ${versionCode} ${params.partner_branch}"
        //                    }
        //                }
        //            }
        //        }
        //     }
        // }
    }
    post {
    //    always {
    //        dir("${project_path}") {
    //            //还原taoyuejia
    //            sh 'git checkout .'
    //        }
    //    }
        success {
            script {
                echo 'success'
    //            def branchInfo = "分支：${params.partner_branch}\\n环境：${params.type}"
    //            def versionInfo = "version：${versionName}#${versionCode}"
    //            def installInfo = "<a href=\\\"${pgyer}\\\">点击直接安装</a>\\n\\n<a href=\\\"${qrCodeUrl}\\\">点击查看二维码</a>\\n\\n<a href=\\\"http://10.8.12.222/apps.html\\\">本地服务器(下载快)</a>"
    //            def msg = "${branchInfo}\\n${versionInfo}\\n${installInfo}"
    //            def postData = '{"appId":"c.111111","subject":"易车伙伴二手车商版打包成功","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
    //            sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
        failure {
            script {
                echo 'failure'
    //            def branchInfo = "分支：${params.partner_branch}\\n环境：${params.type}"
    //            def versionInfo = "version：${versionName}#${versionCode}"
    //            def msg = "${branchInfo}\\n${versionInfo}\\n请检查失败原因"
    //            def postData = '{"appId":"c.111111","subject":"易车伙伴二手车商版打包失败","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
    //            sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
    }
}
