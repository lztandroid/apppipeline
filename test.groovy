//易车伙伴二手车商版打包脚本
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/local/java1.8/jdk1.8.0_271'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        MAVEN_HOME = '/usr/local/maven'
        PATH = "${env.PATH}:$FLUTTER_HOME/bin:$FLUTTER_HOME/bin/cache/dart-sdk/bin:$MAVEN_HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin:${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
        //项目目录
        project_path = 'taochepartner'
        //蒲公英安装链接
        pgyer = ' '
        //二维码链接
        qrCodeUrl = ' '
        versionName = ' '
        versionCode = ' '
        credentialsId="b6d92b26-2529-45ce-aa50-0401644db4d4"
    }

    parameters {
        //此gitParameter参数类型需要安装git Parameter插件才可以生效
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', name: 'partner_branch', type: 'PT_BRANCH', useRepository: '.*taochepartner.git'
        choice(name: 'type', choices: ['qa', 'prodDebug', 'prod'], description: '构建环境')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号，用英文逗号分隔)')
    }
    stages {
        // stage('Init Code') {
        //     steps {
        //         dir("${project_path}") {
        //             //git branch: "${params.partner_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/taochepartner.git'
        //            git branch: "${params.partner_branch}", credentialsId: "${credentialsId}", url: 'git@gitlab.com:lztandroid/apppipeline.git'
        //             script {
        //                 versionName = sh(script: "cat app/build.gradle | awk -F\\\" '/versionName/{print \$2}'", returnStdout: true).trim()
        //                 versionCode = sh(script: "cat app/build.gradle | awk '/versionCode/{print \$2}'", returnStdout: true).trim()
        //                 println versionName
        //                 println versionCode
        //             }        
        //         }
        //     }
        // }

        stage('Init Code') {
            steps {
                dir("${project_path}") {
                    git branch: "${params.partner_branch}", credentialsId: "${credentialsId}", url: 'git@gitlab.com:lztandroid/taochepartner.git'
                }
            }
        }

        stage('打包') {
            steps {
               script {
                        echo '打包'
                    }
            }
        }

    }
}
