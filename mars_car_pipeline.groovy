//小马达打包脚本
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/local/java1.8/jdk1.8.0_271'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        MAVEN_HOME = '/usr/local/maven'
        PATH = "${env.PATH}:$FLUTTER_HOME/bin:$FLUTTER_HOME/bin/cache/dart-sdk/bin:$MAVEN_HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin:${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
        //项目目录
        project_path = 'MarsCar'
        //蒲公英安装链接
        pgyer = ' '
        //二维码链接
        qrCodeUrl = ' '
        credentialsId="a20b1e50-4534-4dcc-a9a7-b820957071f5"
    }
    parameters {
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'release', name: 'mars_branch', type: 'PT_BRANCH', useRepository: '.*MarsCar.git'
        choice(name: 'type', choices: ['qa', 'prod'], description: '构建环境')
        booleanParam(name: 'sendRobot', defaultValue: true, description: '是否发送到打包群')
        booleanParam(name: 'uploadCos', defaultValue: false, description: '上传到腾讯云（prod时勾选，必须确认版本号，否则会覆盖线上包！！！）')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号,用英文逗号分隔,chensisi,liusai,shangyusha,mamingchen,wangquan,anyingying,xulanzhong)')
    }
    stages {
        stage('Init Code') {
            steps {
                dir("${project_path}") {
                    git branch: "${params.mars_branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/MarsCar.git'
                }
                dir("${project_path}") {
                    script {
                        def versionName = getVersionName()
                        def versionCode = getVersionCode()
                        println versionName
                        println versionCode
                        //如果上传腾讯云，判断线上版本是否覆盖
                        if(params.uploadCos) {
                            def response = sh(script:"curl http://p-api.taoche.cn/v1/release/app_bss", returnStdout: true).trim()
                            println response
                            def jsonSlurper = new groovy.json.JsonSlurper()
                            def content = jsonSlurper.parseText(response).data.content
                            def marsCar = null
                            for (int i = 0; i < content.size(); i++) {
                                if(content[i].platform == "新小马达3.0安卓版") {
                                    marsCar = content[i];
                                    break;
                                }
                            }
                            print marsCar
                            if(marsCar?.version == "V${versionName}") {
                                error "线上版本是${versionName}，请核对版本号！！！"
                            }
                        }
                    }
                }
            }
        }
        stage('Build Mars') {
            steps {
                dir("${project_path}") {
                    script {
                        echo '打包'
                        //删除原来的文件
                        sh "rm -rf app/build/outputs/apk"
                        sh './gradlew clean'
                        //打包
                        if (params.type == 'qa') {
                            sh './gradlew app:assembleQaRelease'
                        } else {
                            sh './gradlew app:assembleProdRelease'
                        }
                    }
                }
            }
        }
        stage('上传 apk') {
            parallel {
                stage('上传蒲公英') {
                    steps {
                        script {
                            echo '上传蒲公英'
                            //上传到蒲公英
                            def apkDir = "MarsCar/app/build/outputs/apk"
                            def apkPath = "${apkDir}/${params.type}/release/app-${params.type}-release.apk"
                            //蒲公英key
                            def uKey = "15df770f490d1715e71300ea22cc2ab0"
                            def apiKey = "9dc766c341bd376a74dbb582518d6db6"
                            //蒲公英上传地址
                            def url = "https://www.pgyer.com/apiv2/app/upload"
                            def response = sh(script: "curl -F 'file=@${apkPath}' -F 'uKey=$uKey' -F '_api_key=$apiKey' $url", returnStdout: true).trim()
                            println response
                            def jsonSlurper = new groovy.json.JsonSlurper()
                            def resultData = jsonSlurper.parseText(response).data
                            pgyer = "https://www.pgyer.com/apiv2/app/install?_api_key=$apiKey&buildKey=${resultData.buildKey}"
                            qrCodeUrl = resultData.buildQRCodeURL
                        }
                    }
                }
                
                stage('上传本地') {
                    steps {
                        dir("${project_path}") {
                            script {
                                echo '上传本地服务器'
                                def versionName = getVersionName()
                                def versionCode = getVersionCode()
                                //上传本地服务器
                                def apkPath = "app/build/outputs/apk/${params.type}/release/app-${params.type}-release.apk"
                                sh "mkdir -p 1_AutoArchive/apks"
                                sh "cp -rf $apkPath 1_AutoArchive/apks/app.apk"
                                sh "1_AutoArchive/4.1_upload_apk.sh MarsCarAndroid"
                                sh "1_AutoArchive/5.1_updateHTML.sh MarsCarAndroid 小马达 xiaomada_android.png ${versionName} ${versionCode} ${params.mars_branch}"

                                //上传腾讯云
                                if(params.uploadCos) {
                                    dir('util') {
                                        def out = sh(script:"ls node_modules", returnStatus:true)
                                        //文件夹不存在
                                        if(out == 2) {
                                            sh "npm install"
                                        }
                                    }
                                    def target = "/marscar/${type}/marscar-${type}-${versionName}-${versionCode}.apk"
                                    sh "node util/5_uploadCos.js ${apkPath} ${target}"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            dir("${project_path}") {
                //还原MarsCar
                sh 'git checkout .'
            }
            dir('taoche_mars_flutter') {
                //还原flutter
                sh 'git checkout .'
            }
        }
        success {
            dir("${project_path}") {
                script {
                    echo 'success'
                    def versionName = getVersionName()
                    def versionCode = getVersionCode()
                    def branchInfo = "MarsCar分支：${params.mars_branch}\\n环境：${params.type}"
                    def versionInfo = "version：${versionName}#${versionCode}"
                    def installInfo = "<a href=\\\"${pgyer}\\\">点击直接安装</a>\\n\\n<a href=\\\"${qrCodeUrl}\\\">点击查看二维码</a>\\n\\n<a href=\\\"http://10.8.12.222/apps.html\\\">本地服务器(下载快)</a>"
                    def msg = "${branchInfo}\\n${versionInfo}\\n${installInfo}"
                    def target = "/marscar/${type}/marscar-${type}-${versionName}-${versionCode}.apk"
                    def apkUrl = "https://taoche-res.taoche.com${target}"
                    if(params.uploadCos) {
                        msg = "${msg}\\n\\n服务器下载地址：${apkUrl}"
                    }
                    def postData = '{"appId":"c.111111","subject":"小马达打包成功","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                    sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
                    //机器人通知
                    if(params.sendRobot) {
                        def content = "### 小马达Android打包成功\n" +
                            ">MarsCar分支：<font color=\"warning\">${params.mars_branch}</font>\n" +
                            "环境：<font color=\"warning\">${params.type}</font>\n" +
                            "version：<font color=\"info\">${versionName}#${versionCode}</font>\n" +
                            "##### [点击直接安装](${pgyer})\n" +
                            "##### [点击查看二维码](${qrCodeUrl})\n" +
                            "##### [本地服务器(下载快)](http://10.8.12.222/apps.html)\n"
                        if(params.uploadCos) {
                            content = "${content}\n##### 服务器下载地址：${apkUrl}"
                        }
                        def wechat = groovy.json.JsonOutput.toJson(["msgtype" : "markdown", "markdown": ["content": "$content"]])
                        //sh "curl -H 'Content-Type:application/json' -d '$wechat' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=26c984b5-3992-49b5-a3ae-da6e616d4a35'"
                        sh "curl -H 'Content-Type:application/json' -d '$wechat' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ae3604f0-7fbe-4255-bebe-4fc003ea1a47'"
                    }
                }
            }
        }
        failure {
            dir("${project_path}") {
                script {
                    echo 'failure'
                    def versionName = getVersionName()
                    def versionCode = getVersionCode()
                    def branchInfo = "MarsCar分支：${params.mars_branch}\\n环境：${params.type}"
                    def versionInfo = "version：${versionName}#${versionCode}"
                    def msg = "${branchInfo}\\n${versionInfo}\\n请检查失败原因"
                    def postData = '{"appId":"c.111111","subject":"小马达打包失败","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                    sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
                }
            }
        }
    }
}

def getVersionName() {
    def versionName = sh(script: "cat app/build.gradle | awk -F\\\" '/versionName/{print \$2}'", returnStdout: true).trim()
    return versionName
}

def getVersionCode() {
    def versionCode = sh(script: "cat app/build.gradle | awk '/versionCode/{print \$2}'", returnStdout: true).trim()
    return versionCode
} 