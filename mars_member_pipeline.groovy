//小马达会员打包脚本
pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/local/java1.8/jdk1.8.0_271'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        MAVEN_HOME = '/usr/local/maven'
        PATH = "${env.PATH}:$FLUTTER_HOME/bin:$FLUTTER_HOME/bin/cache/dart-sdk/bin:$MAVEN_HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin:${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
        //项目目录
        project_path = 'marsmember'
        //小马达主项目分支
        mars_branch = 'feature_member'
        //小马达主项目flutter分支
        flutter_branch = 'ui_xb'
        credentialsId="a20b1e50-4534-4dcc-a9a7-b820957071f5"
    }
    parameters {
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'master', name: 'branch', type: 'PT_BRANCH', useRepository: '.*marsmember.git'
        choice(name: 'type', choices: ['qa', 'prod'], description: '构建环境')
        booleanParam(name: 'uploadCos', defaultValue: false, description: '上传到腾讯云（prod时勾选，必须确认版本号，否则会覆盖线上包！！！）')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号,用英文逗号分隔,chensisi,liusai,shangyusha,mamingchen,wangquan,anyingying,xulanzhong)')
    }
    stages {
        stage('Init Code') {
            steps {
                dir("${project_path}") {
                    git branch: "${params.branch}", credentialsId: "${credentialsId}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/marsmember.git'
                }
            }
        }
        stage('Build Mars Member') {
            steps {
                dir("${project_path}") {
                    script {
                        echo '打包'
                        //sh './gradlew clean'
                        //打包
                        if (params.type == 'qa') {
                            sh './gradlew module_member:publishForTest'
                        } else {
                            //sh './gradlew module_member:publishForTest'
                        }
                    }
                }
            }
        }
        stage('Update Mars File') {
            when { 
                expression { return params.type == 'qa' }
            }
            steps {
                dir("${project_path}") {
                    script {
                        //获取小马达build.gradle内容
                        def marsBuildFile = "curl -X GET --header 'PRIVATE-TOKEN: qqRvwk9soHnY8z-Exb-C' 'http://git.taoche.com/api/v4/projects/1161/repository/files/app%2Fbuild.gradle/raw?ref=${mars_branch}'"
                        def marsBuildContent = sh(script: marsBuildFile, returnStdout: true).trim()
                        //println marsBuildContent
                        def new_version = sh(script: "cat module_member/module_version.txt", returnStdout: true).trim()
                        //println new_version
                        //更新会员版本
                        def newMarsBuildContent = marsBuildContent.replaceAll("com.taoche.mars:module_member:.*", "${new_version}'")
                        def putData = groovy.json.JsonOutput.toJson(["branch":"${mars_branch}","content":"${newMarsBuildContent}","commit_message":"自动打包"])
                        putData = putData.replaceAll("'", "'\\\\''")
                        //println putData
                        //修改内容
                        sh "curl -X PUT --header 'PRIVATE-TOKEN: qqRvwk9soHnY8z-Exb-C' --header 'Content-Type: application/json' --data '${putData}' 'http://git.taoche.com/api/v4/projects/1161/repository/files/app%2Fbuild.gradle'"
                    }
                }
            }
        }

        stage('Build Mars') {
            when { 
                equals expected: 'qa', 
                actual: params.type
            }
            steps {
                script {
                    //调用小马达打包
                    def params = "mars_branch=${mars_branch}&flutter_branch=${flutter_branch}&type=${params.type}&wxusers=${params.wxusers}"
                    sh "curl -X POST --user tcauto:1199f42898c41dec59d1d3e204e9c51b70 'http://10.8.100.62/jenkins/job/MarsCar/buildWithParameters?${params}'"
                }
            }
        }
    }
    post {
        success {
            echo 'success'
        }

        failure {
            dir("${project_path}") {
                script {
                    echo 'failure'
                    def branchInfo = "MarsMember分支：${params.branch}\\n环境：${params.type}"
                    def msg = "${branchInfo}\\n请检查失败原因"
                    def postData = '{"appId":"c.111111","subject":"小马达会员打包失败","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                    sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
                }
            }
        }
    }
}