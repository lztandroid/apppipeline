pipeline {
    agent any
    environment {
        //配置环境变量
        JAVA_HOME = '/usr/lib/jvm/java'
        JRE_HOME = "$JAVA_HOME/jre"
        PUB_HOSTED_URL = 'https://pub.flutter-io.cn'
        FLUTTER_STORAGE_BASE_URL = 'https://storage.flutter-io.cn'
        FLUTTER_HOME = '/usr/local/flutter'
        ANDROID_HOME = '/usr/local/android_sdk'
        PATH = "${env.PATH}:$FLUTTER_HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin:${ANDROID_HOME}/build-tools/29.0.3:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools"
        //蒲公英安装链接
        pgyer = ""
        //二维码链接
        qrCodeUrl = ""
    }
    parameters {
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'release', name: 'mars_branch', type: 'PT_BRANCH', useRepository: '.*MarsCar.git'
        gitParameter branchFilter: 'origin.*/(.*)', defaultValue: 'release', name: 'flutter_branch', type: 'PT_BRANCH', useRepository: '.*taoche_mars_flutter.git'
        choice(name: 'type', choices: ['Qa', 'Prod'], description: '构建环境')
        string(name: 'wxusers', defaultValue: 'lixianpeng', description: '通知账号(域账号，用英文逗号分隔)')
    }
    stages {
        stage('Init Code') {
            steps {
                dir('MarsCar') {
                    git branch: "${params.mars_branch}", url: 'ssh://git@git.taoche.com:52000/Business_App_Android/MarsCar.git'
                }
                dir('taoche_mars_flutter') {
                    git branch: "${params.flutter_branch}", url: 'ssh://git@git.taoche.com:52000/Business_App/Flutter/taoche_mars_flutter.git'
                }
            }
        }
        stage('Build flutter') {
            steps {
                dir('taoche_mars_flutter') {
                    script {
                        //去除framework依赖
                        sh "sed -i '/taoche_flutter_framework:/,/version:.*\$/d' pubspec.yaml"
                        //sh 'flutter packages get'
                        //还原
                        //sh 'git checkout .'
                        //重新获取flutter依赖
                        //sh 'flutter packages get'
                        //打aar包
                        echo 'build aar'
                        //sh 'flutter build aar  --no-debug --no-profile --target-platform android-arm'
                        def now_version = sh(script: "awk -F\\\" '/targetVersion.*=/{print \$2}' flutter_aar.dart", returnStdout: true).trim()
                        def commitId = sh(script: "git rev-parse --short=5 HEAD", returnStdout: true).trim()
                        def new_version = "0.0.1-auto-$commitId"
                        //设置aar版本号
                        sh "sed -i 's/$now_version/$new_version/' flutter_aar.dart"
                        //设置MarsCar aar打包
                        sh "sed -i 's/isFlutterAar=.*/isFlutterAar=true/' ../MarsCar/gradle.properties"
                        //更新MarsCar依赖版本号
                        sh "sed -i 's/taocheFlutterVersion.*:.*/taocheFlutterVersion    : \\\"$new_version\\\",/' ../MarsCar/config.gradle"
                        //上传aar到maven
                        //sh 'dart flutter_aar.dart'
                    }
                }
            }
        }
        stage('Build Mars') {
            steps {
                dir('MarsCar') {
                    script {
                        echo '打包'
                        //打包
                        // if (params.type == 'Qa') {
                        //     sh './gradlew app:assembleQaReleaseDebuggable'
                        // } else {
                        //     sh './gradlew app:assembleProdRelease'
                        // }
                    }
                }
            }
        }
        stage('上传 apk') {
            parallel {
                stage('上传蒲公英') {
                    steps {
                        script {
                            echo '上传蒲公英'
                            //上传到蒲公英
                            // def apkDir = "${WORKSPACE}/MarsCar/app/build/outputs/apk"
                            // def apkPath = ''
                            // if (params.type == 'Qa') {
                            //     apkPath = "${apkDir}/qa/releaseDebuggable/app-qa-releaseDebuggable.apk"
                            // } else {
                            //     apkPath = "${apkDir}/prod/release/app-prod-release.apk"
                            // }
                            // //蒲公英key
                            // def uKey = "15df770f490d1715e71300ea22cc2ab0"
                            // def apiKey = "9dc766c341bd376a74dbb582518d6db6"
                            // //蒲公英上传地址
                            // def url = "https://www.pgyer.com/apiv2/app/upload"
                            // def response = sh(script: "curl -F 'file=@${apkPath}' -F 'uKey=$uKey' -F '_api_key=$apiKey' $url", returnStdout: true).trim()
                            // println response
                            // def jsonSlurper = new groovy.json.JsonSlurper()
                            // def resultData = jsonSlurper.parseText(response).data
                            // pgyer = "https://www.pgyer.com/apiv2/app/install?_api_key=$apiKey&appKey=${resultData.buildShortcutUrl}"
                            // qrCodeUrl = resultData.buildQRCodeURL
                        }
                    }
                }
                
                stage('上传本地') {
                    steps {
                        dir('MarsCar') {
                            script {
                                echo '上传本地服务器'
                                //上传本地服务器
                                // def apkPath = ''
                                // if (params.type == 'Qa') {
                                //     apkPath = "app/build/outputs/apk/qa/releaseDebuggable/app-qa-releaseDebuggable.apk"
                                // } else {
                                //     apkPath = "app/build/outputs/apk/prod/release/app-prod-release.apk"
                                // }
                                // sh "cp -rf $apkPath 1_AutoArchive/apks/app.apk"
                                // def versionName = sh(script: "cat app/build.gradle | awk -F\\\" '/versionName/{print \$2}'", returnStdout: true).trim()
                                // def versionCode = sh(script: "cat app/build.gradle | awk '/versionCode/{print \$2}'", returnStdout: true).trim()
                                // println versionName
                                // println versionCode
                                // sh "1_AutoArchive/4.1_upload_apk.sh MarsCarAndroid"
                                // sh "1_AutoArchive/5.1_updateHTML.sh MarsCarAndroid 小马达 xiaomada_android.png ${versionName} ${versionCode} ${params.mars_branch}"
                           
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            dir('MarsCar') {
                //还原MarsCar
                //sh 'git checkout .'
            }
            dir('taoche_mars_flutter') {
                //还原flutter
                //sh 'git checkout .'
            }
        }
        success {
            script {
                //def msg = "MarsCar分支：${params.mars_branch}\\nflutter分支：${params.flutter_branch}\\n<a href=\\\"$pgyer\\\">点击直接安装</a>\\n\\n<a href=\\\"$qrCodeUrl\\\">点击查看二维码</a>"
                //def postData = '{"appId":"c.111111","subject":"小马达打包成功","wxusers":"' + params.wxusers + '","attch":0,"text":"' + msg + '","args":"1000006"}'
                //sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
        failure {
            script {
                def postData = '{"appId":"c.111111","subject":"小马达打包失败","wxusers":"' + params.wxusers + '","attch":0,"text":"请检查失败原因","args":"1000006"}'
                //sh "curl -H 'Content-Type:application/json' -X POST -d '$postData' http://b2c-qa.kanche.com/baseApi/messageapi/send"
            }
        }
    }
}